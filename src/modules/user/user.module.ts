import { Module } from '@nestjs/common';
import { UserController } from 'src/controllers/user/user.controller';
import { UserService } from 'src/services/user/user.service'
import { UserRepository } from 'src/repositories/user/user.repository';
import { MongooseModule } from "@nestjs/mongoose";
import { User, UserSchema } from 'src/schemas/user/user.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])],
    controllers: [UserController],
    providers: [UserService, UserRepository]
})
export class UserModule {}
