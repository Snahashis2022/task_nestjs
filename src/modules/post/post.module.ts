import { Module } from '@nestjs/common';
import { PostController } from 'src/controllers/post/post.controller';
import { PostService } from 'src/services/post/post.service'
import { PostRepository } from 'src/repositories/post/post.repository';
import { MongooseModule } from "@nestjs/mongoose";
import { Post, PostSchema } from 'src/schemas/post/post.schema';
@Module({
    imports: [MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }])],
    controllers: [PostController],
    providers: [PostService, PostRepository]
})
export class PostModule {}
