import { Module } from '@nestjs/common';
import { CommentController } from 'src/controllers/comment/comment.controller';
import { MongooseModule } from "@nestjs/mongoose";

import { CommentService } from 'src/services/comment/comment.service'
import { Comment, CommentSchema } from 'src/schemas/comment/comment.schema';
import { CommentRepository } from 'src/repositories/comment/comment.repository';

import { Post, PostSchema } from 'src/schemas/post/post.schema';
import { PostRepository } from 'src/repositories/post/post.repository';
@Module({
    imports: [MongooseModule.forFeature([{ name: Comment.name, schema: CommentSchema }]),
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }])],
    controllers: [CommentController],
    providers: [CommentService, CommentRepository,  PostRepository]
})
export class CommentModule {}
