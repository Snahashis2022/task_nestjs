import { IsOptional } from 'class-validator';
export class CreateCommentDto{
    postId: string;
    name: string;
    email: string;
    body: string;
}