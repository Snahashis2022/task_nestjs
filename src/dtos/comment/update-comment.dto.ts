import { IsOptional } from 'class-validator';
export class UpdateCommentDto{
    @IsOptional()
    name: string;

    @IsOptional()
    email: string;
    
    @IsOptional()
    body: string;
}