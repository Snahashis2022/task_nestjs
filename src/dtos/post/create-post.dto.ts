import { IsOptional } from 'class-validator';
export class CreatePostDto{
    userId: string;
    title: string;
    body: string;
}