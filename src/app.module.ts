import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { MongooseModule } from '@nestjs/mongoose';

import { MessagesModule } from './messages/messages.module';
import { UserModule } from './modules/user/user.module';
import { ProductModule } from './modules/product/product.module';
import { UserService } from './services/user/user.service';
import { UserController } from './controllers/user/user.controller';
import { PostModule } from './modules/post/post.module';
import { CommentModule } from './modules/comment/comment.module';
import { PostController } from './controllers/post/post.controller';
import { CommentController } from './controllers/comment/comment.controller';
import { PostService } from './services/post/post.service';
import { CommentService } from './services/comment/comment.service';


@Module({
  imports: [MongooseModule.forRoot(
    "mongodb+srv://McSpraint:spraintswebtech@cluster0.liez7.mongodb.net/test?retryWrites=true&w=majority"
  ), MessagesModule, UserModule, ProductModule, PostModule, CommentModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
