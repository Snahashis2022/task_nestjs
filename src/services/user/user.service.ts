import { Injectable } from "@nestjs/common";
//import { v4 as uuidv4 } from 'uuid';
import { UpdateUserDto } from "./../../dtos/user/update-user.dto";

import { User } from "./../../schemas/user/user.schema"
import { UserRepository } from "./../../repositories/user/user.repository";

@Injectable()
export class UserService {
    constructor(private readonly userRepository: UserRepository) {}

    async getUserById(userId: string): Promise<User> {
        try{
            const usr = await this.userRepository.findOne({ userId });
            return usr;
        }catch(err){
            throw err;
        }
    }

    async getUsers(): Promise<User[]> {
        try{
            const usrs = await this.userRepository.find({});
            return usrs;
        }catch(err){
            throw err;
        }
        
    }

    async createUser(name: string, email: string, password: string): Promise<User> {
        try{
        const usr = await this.userRepository.create({
            name,
            email,
            password,
        });
        return usr;
        }catch(err){
            throw err;
        }
    }

    async updateUser(userId: string, userUpdates: UpdateUserDto): Promise<User> {
        try{
            const usrs = await this.userRepository.findOneAndUpdate({ userId }, userUpdates);
            return usrs;
        }catch(err){
            throw err;
        }
    }
}
