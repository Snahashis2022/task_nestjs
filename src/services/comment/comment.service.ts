import { Injectable } from "@nestjs/common";
//import { v4 as uuidv4 } from 'uuid';
import { UpdateCommentDto } from "./../../dtos/comment/update-comment.dto";

import { Comment } from "./../../schemas/comment/comment.schema"
import { CommentRepository } from "./../../repositories/comment/comment.repository";

@Injectable()
export class CommentService {
    constructor(private readonly commentRepository: CommentRepository){}

    async getCommentById(_id: string): Promise<Comment> {
        try{
            const cmnt = await this.commentRepository.findOne({ _id });
            return cmnt;
        }catch(err){
            throw err;
        }
    }

    async getComments(): Promise<Comment[]> {
        try{
            const cmnts = await this.commentRepository.find({});
            return cmnts;
        }catch(err){
            throw err;
        }
        
    }

    async createComment(postId: string, name: string, email: string, body: string): Promise<Comment> {
        try{
        const cmnt = await this.commentRepository.create({
            postId,
            name,
            email,
            body
        });
        return cmnt;
        }catch(err){
            throw err;
        }
    }

    async updateComment(_id: string, updatedComment: UpdateCommentDto): Promise<Comment> {
        try{
            const cmnt = await this.commentRepository.findOneAndUpdate({ _id }, updatedComment);
            return cmnt;
        }catch(err){
            throw err;
        }
    }
}


