import { Injectable } from "@nestjs/common";
//import { v4 as uuidv4 } from 'uuid';
import { UpdatePostDto } from "./../../dtos/post/update-post.dto";

import { Post } from "./../../schemas/post/post.schema"
import { PostRepository } from "./../../repositories/post/post.repository";

@Injectable()
export class PostService {
    constructor(private readonly postRepository: PostRepository) {}

    async getPostById(_id: string): Promise<Post> {
        try{
            const pst = await this.postRepository.findOne({ _id });
            return pst;
        }catch(err){
            throw err;
        }
    }

    async getPosts(): Promise<Post[]> {
        try{
            const psts = await this.postRepository.find({});
            return psts;
        }catch(err){
            throw err;
        }
        
    }

    async createPost(userId: string, title: string, body: string): Promise<Post> {
        try{
        const pst = await this.postRepository.create({
            userId,
            title,
            body,
        });
        return pst;
        }catch(err){
            throw err;
        }
    }

    async updatePost(_id: string, updatedPost: UpdatePostDto): Promise<Post> {
        try{
            const pst = await this.postRepository.findOneAndUpdate({ _id }, updatedPost);
            return pst;
        }catch(err){
            throw err;
        }
    }

    async getPostsWithComments(): Promise<any>{
        try{
            const psts = await this.postRepository.find({});
            //const cmts=psts.forEach(post =>{this.commentRepository.find({postId:post._id})})
            //const cmnts = await this.commentRepository.find({})
            return psts;
        }catch(err){
            throw err;
        }
    }
}

