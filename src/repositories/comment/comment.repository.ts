import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, FilterQuery } from "mongoose";
import { Comment, CommentDocument } from './../../schemas/comment/comment.schema';
import { CreateCommentDto } from "src/dtos/comment/create-comment.dto";

import { Post, PostDocument } from "src/schemas/post/post.schema";
@Injectable()
export class CommentRepository {
    constructor(
        @InjectModel(Comment.name) 
        private commentModel: Model<CommentDocument>,
        @InjectModel(Post.name) 
        private postModel: Model<PostDocument>,
        ){

        }

    async findOne(commentFilterQuery: FilterQuery<Comment>): Promise<Comment>{
        return this.commentModel.findOne(commentFilterQuery);
    }
    async find(commentFilterQuery: FilterQuery<Comment>): Promise<Comment[]>{
        return this.commentModel.find(commentFilterQuery);
    }
    async create(comment:CreateCommentDto): Promise<Comment>{
        const newComment = new this.commentModel(comment);
        const savedComment = await newComment.save();
        let a = await this.postModel.findOneAndUpdate({postId: savedComment.postId}, { $push: { comments: savedComment._id } }, {new: true});
        console.log(a);
        return savedComment;
    }
    async findOneAndUpdate(postFilterQuery: FilterQuery<Comment>, comment:Partial<Comment>): Promise<Comment>{// Partial stands for partial properties of a class type
        return this.commentModel.findOneAndUpdate(postFilterQuery, comment);
    }
}