import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, FilterQuery } from "mongoose";
import { User, UserDocument } from './../../schemas/user/user.schema';
import { CreateUserDto } from "src/dtos/user/create-user.dto";

@Injectable()
export class UserRepository {
    constructor(
        @InjectModel(User.name) 
        private userModel: Model<UserDocument>
        ){

        }

    async findOne(userFilterQuery: FilterQuery<User>): Promise<User>{
        return this.userModel.findOne(userFilterQuery);
    }
    async find(userFilterQuery: FilterQuery<User>): Promise<User[]>{
        return this.userModel.find(userFilterQuery);
    }
    async create(user:CreateUserDto): Promise<User>{
        const newUser = new this.userModel(user);
        return newUser.save();
    }
    async findOneAndUpdate(userFilterQuery: FilterQuery<User>, user:Partial<User>): Promise<User>{// Partial stands for partial properties of a class type
        return this.userModel.findOneAndUpdate(userFilterQuery, user);
    }
}