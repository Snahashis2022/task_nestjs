import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, FilterQuery } from "mongoose";
import { Post, PostDocument } from './../../schemas/post/post.schema';
import { CreatePostDto } from "src/dtos/post/create-post.dto";

@Injectable()
export class PostRepository {
    constructor(
        @InjectModel(Post.name) 
        private postModel: Model<PostDocument>
        ){

        }

    async findOne(postFilterQuery: FilterQuery<Post>): Promise<Post>{
        return this.postModel.findOne(postFilterQuery);
    }
    async find(postFilterQuery: FilterQuery<Post>): Promise<Post[]>{
        return this.postModel.find(postFilterQuery).populate('comments');
    }
    async create(post:CreatePostDto): Promise<Post>{
        const newPost = new this.postModel(post);
        return newPost.save();
    }
    async findOneAndUpdate(postFilterQuery: FilterQuery<Post>, post:Partial<Post>): Promise<Post>{// Partial stands for partial properties of a class type
        return this.postModel.findOneAndUpdate(postFilterQuery, post);
    }
}