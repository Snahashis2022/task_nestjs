import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

export type PostDocument = Post & Document;

@Schema({collection:"postCollection", timestamps: true})
export class Post extends Document{
    @Prop({type: SchemaTypes.ObjectId, required: true, ref:"User"})
    userId: Types.ObjectId;

    @Prop({type: String, required: true, unique: true})
    title: String;

    @Prop({type: String, required: true})
    body: String;

    @Prop({type: [SchemaTypes.ObjectId], ref:"Comment"})
    comments: Types.ObjectId[];
    
}
export const PostSchema = SchemaFactory.createForClass(Post);