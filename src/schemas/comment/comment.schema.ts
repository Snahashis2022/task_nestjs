import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes, Types } from 'mongoose';

export type CommentDocument = Comment & Document;

@Schema({collection:"commentCollection", timestamps: true})
export class Comment extends Document{
    @Prop({required: true, type: SchemaTypes.ObjectId, ref:"Post"})
    postId: Types.ObjectId;

    @Prop({required: true, unique: true})
    name: String;

    @Prop({required: true, unique: true})
    email: String;

    @Prop({required: true})
    body: String;
    
}
export const CommentSchema = SchemaFactory.createForClass(Comment);