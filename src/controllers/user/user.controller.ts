import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { User } from './../../schemas/user/user.schema';
import { UserService } from './../../services/user/user.service';

import { CreateUserDto } from 'src/dtos/user/create-user.dto';
import { UpdateUserDto } from 'src/dtos/user/update-user.dto';


@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}
    
    @Get('/:userId')
    async getUser(@Param('userId') userId: string): Promise<User> {
    return this.userService.getUserById(userId);
    }

    @Get()
    async getUsers(): Promise<User[]> {
        return this.userService.getUsers();
    }

    @Post()
    async createUser(@Body() createUserDto: CreateUserDto): Promise<User> {
        return this.userService.createUser(createUserDto.name, createUserDto.email, createUserDto.password)
    }

    @Patch('/:userId')
    async updateUser(@Param('userId') userId: string, @Body() updateUserDto: UpdateUserDto): Promise<User> {
        return this.userService.updateUser(userId, updateUserDto);
    }
}
