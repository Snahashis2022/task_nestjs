import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { Comment } from './../../schemas/comment/comment.schema';
import { CommentService } from './../../services/comment/comment.service';

import { CreateCommentDto } from 'src/dtos/comment/create-comment.dto';
import { UpdateCommentDto } from 'src/dtos/comment/update-comment.dto';


@Controller('comments')
export class CommentController {
    constructor(private readonly commentService: CommentService) {}
    
    @Get('/:commentId')
    async getPost(@Param('commentId') commentId: string): Promise<Comment> {
    return this.commentService.getCommentById(commentId);
    }

    @Get()
    async getPosts(): Promise<Comment[]> {
        return this.commentService.getComments();
    }

    @Post()
    async createPost(@Body() createdComment: CreateCommentDto): Promise<Comment> {
        return this.commentService.createComment(createdComment.postId, createdComment.name, createdComment.email, createdComment.body)
    }

    @Patch('/:postId')
    async updatePost(@Param('postId') _id: string, @Body() updatedComment: UpdateCommentDto): Promise<Comment> {
        return this.commentService.updateComment(_id, updatedComment);
    }
}
