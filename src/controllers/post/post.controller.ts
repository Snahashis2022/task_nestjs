import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { Post as Posts } from './../../schemas/post/post.schema';
import { PostService } from './../../services/post/post.service';

import { CreatePostDto } from 'src/dtos/post/create-post.dto';
import { UpdatePostDto } from 'src/dtos/post/update-post.dto';


@Controller('posts')
export class PostController {
    constructor(private readonly postService: PostService) {}
    
    @Get('/:postId')
    async getPost(@Param('postId') postId: string): Promise<Posts> {
    return this.postService.getPostById(postId);
    }

    @Get()
    async getPosts(): Promise<Posts[]> {
        return this.postService.getPosts();
    }

    @Post()
    async createPost(@Body() createdPost: CreatePostDto): Promise<Posts> {
        return this.postService.createPost(createdPost.userId, createdPost.title, createdPost.body)
    }

    @Patch('/:postId')
    async updatePost(@Param('postId') _id: string, @Body() updatedPost: UpdatePostDto): Promise<Posts> {
        return this.postService.updatePost(_id, updatedPost);
    }
    @Get('/all')
    async getPostsWithComments(): Promise<any>{
        return this.postService.getPostsWithComments();
    }
}