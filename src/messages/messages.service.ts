import { Injectable } from '@nestjs/common';

import { MessagesRepository } from './message.repo';

@Injectable()
export class MessagesService {

    constructor(public _msgRepo:MessagesRepository) {}

    findOne(id: string){
        return this._msgRepo.findOne(id);
    }
    findAll(){
        return this._msgRepo.findAll();
    }
    postMessage(content: string){
        return this._msgRepo.create(content);
    }
}
