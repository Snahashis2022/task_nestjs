import { IsObject, IsString } from "class-validator";

export class PostMessageDto{
    @IsString()
    content: String;
}