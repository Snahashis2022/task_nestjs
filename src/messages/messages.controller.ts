import { 
    Controller, 
    Get, 
    Post, 
    Body, 
    Param, 
    Query, 
    NotFoundException} from '@nestjs/common';
import { PostMessageDto } from './message.dto';
import { MessagesService } from './messages.service';

@Controller('messages')
export class MessagesController {
    constructor(private _msg:MessagesService){}
    @Get()
    listMessages(): Object {
        return this._msg.findAll(); 
    }
    @Get("/:id")
    async getMessages(@Param('id') id: string): Promise<any> { 
        const msg = await this._msg.findOne(id);
        if(!msg) throw new NotFoundException("Not Found");
        return msg;
    }
    @Post()
    async postMessages(@Body() body:string): Promise<any> {
        console.log(body);
        this._msg.postMessage(body)
    }
    
}
