import { readFile, writeFile } from "fs/promises";
import { Injectable } from '@nestjs/common';
@Injectable()
export class MessagesRepository {
    async findOne(id: string): Promise<any>{
        const contents = await readFile("messages.json", "utf8");
        const messages = JSON.parse(contents);
        return messages[id];
    }
    async findAll(): Promise<any> {
        const contents = await readFile("messages.json", "utf8");
        const messages = JSON.parse(contents);

        return messages;
    }
    async create(content: string): Promise<any> {
        const contents = await readFile("messages.json", "utf8");
        const messages = JSON.parse(contents);
        const id = Math.floor(Math.random() * 99);
        messages[id] = {id, content};
        return await writeFile("messages.json", JSON.stringify(messages));
        
    }
}